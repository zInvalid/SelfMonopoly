package de.mia19.game.fields;

import de.mia19.game.Game;
import de.mia19.game.objects.Player;

public abstract class Field
{
    private final int position;
    //private final FieldType fieldType;
    private int location, coord;
    private String name;

    public static final int NORTH = 0;
    public static final int SOUTH = 1;
    public static final int EAST = 2;
    public static final int WEST = 3;

    public Field(String name, int location, int coord)
    {
        this.name = name;
        this.location = location;
        this.coord = coord;
        this.position = Game.getInstance().getFields().size();
        Game.getInstance().getFields().add(this);
    }

    public int getPosition()
    {
        return position;
    }

    public static Field getFromNumber(int number)
    {
        return Game.getInstance().getFields().get(number);
    }

    public static Field getFromCoords(int location, int coord)
    {
        for(Field field : Game.getInstance().getFields())
        {
            if(field.getLocation() == location && field.getCoord() == coord)
                return field;
        }

        return null;
    }

    public int getCoord()
    {
        return coord;
    }

    public int getLocation()
    {
        return location;
    }

    public String getName()
    {
        return name;
    }

    /**
     * Executes specific code for the respective field
     */
    public abstract void action(Player player);
}

