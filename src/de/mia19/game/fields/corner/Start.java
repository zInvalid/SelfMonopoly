package de.mia19.game.fields.corner;

import de.mia19.game.fields.Field;
import de.mia19.game.objects.Player;

public class Start extends Field
{

    public Start(String name, int location, int coord)
    {
        super(name, location, coord);
    }

    @Override
    public void action(Player player)
    {

    }
}
