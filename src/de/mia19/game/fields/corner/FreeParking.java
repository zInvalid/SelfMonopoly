package de.mia19.game.fields.corner;

import de.mia19.game.Config;
import de.mia19.game.Game;
import de.mia19.game.fields.Field;
import de.mia19.game.fields.propertys.Plant;
import de.mia19.game.objects.Player;

public class FreeParking extends Field
{
    public FreeParking(String name, int location, int coord)
    {
        super(name, location, coord);
    }

    @Override
    public void action(Player player)
    {
        if (Config.getInstance().isFree_parking())
            Game.getInstance().activePlayer.addMoney(Game.getInstance().tax);
    }
}
