package de.mia19.game.fields.other;

import de.mia19.game.fields.Field;
import de.mia19.game.objects.Player;

public class Tax extends Field
{
    private int tax;

    public Tax(String name, int tax, int location, int coord)
    {
        super(name, location, coord);
        this.tax = tax;
    }

    @Override
    public void action(Player player)
    {
        player.pay_tax(tax);
    }
}
