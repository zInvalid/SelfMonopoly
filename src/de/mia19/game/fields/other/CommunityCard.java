package de.mia19.game.fields.other;

import de.mia19.game.fields.Field;
import de.mia19.game.objects.Player;

public class CommunityCard extends Field
{
    public CommunityCard(String name, int location, int coord)
    {
        super(name, location, coord);
    }

    @Override
    public void action(Player player)
    {

    }
}
