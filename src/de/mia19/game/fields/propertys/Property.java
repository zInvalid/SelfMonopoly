package de.mia19.game.fields.propertys;

import de.mia19.game.Game;
import de.mia19.game.fields.Field;
import de.mia19.game.objects.Player;

public class Property extends Field
{
    private Player owner;

    private final long price;
    private long rent;

    public Property(String name, long price, long rent, int location, int coord)
    {
        super(name, location, coord);

        this.owner = null;

        this.price = price;
        this.rent = rent;
    }

    public boolean isAlreadyBought()
    {
        return owner != null;
    }

    public long getRent(Player player)
    {
        return rent;
    }

    public Player getOwner()
    {
        return owner;
    }

    public long getPrice()
    {
        return price;
    }

    public boolean hasOwner()
    {
        return owner != null;
    }

    public boolean isOwner(Player player)
    {
        return owner.equals(player);
    }

    public void setOwner(Player owner)
    {
        this.owner = owner;
    }


    @Override
    //TODO Messages
    public void action(Player player)
    {
        if(!hasOwner())
        {
            //Fragen ob kaufen will
            if(true)
            {
                if(player.buyProperty(this))
                {
                    //SUCCESSFULL
                }
                else
                {
                    //NOT ENOUGH MONEY
                }
            }
        }
        else
        {
            if(!isOwner(player))
            {
                player.pay(getOwner(), getRent(player));
            }
        }
    }

}
