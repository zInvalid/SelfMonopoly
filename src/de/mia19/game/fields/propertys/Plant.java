package de.mia19.game.fields.propertys;

import de.mia19.game.objects.Player;

public class Plant extends Property
{
    public Plant(String name, long price, long rent, int location, int coord)
    {
        super(name, price, rent, location, coord);
    }

    @Override
    public long getRent(Player player)
    {
        if(super.getOwner().getOwnedByType(Plant.class).size() == 2)
            return player.getDice().getValue() * 10;
        else
            return player.getDice().getValue() * 4;
    }
}
