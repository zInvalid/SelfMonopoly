package de.mia19.game.fields.propertys;

import de.mia19.game.*;
import de.mia19.game.fields.Field;
import de.mia19.game.objects.Player;


public class Street extends Property
{
    private int houseCount;
    private final long housePrice;
    private final GameColor streetGameColor;

    public Street(String name, long price, long rent, GameColor streetGameColor, long housePrice, int location, int coord)
    {
        super(name, price, rent, location, coord);
        this.streetGameColor = streetGameColor;
        this.housePrice = housePrice;
    }

    /**
     * @return the amount of Streets with the same GameColor
     * @see GameColor
     */
    private int getAmountOfStreets()
    {
        int i = 0;
        for (Field field : Game.getInstance().getFields())
        {
            if (field instanceof Property)
            {
                if ((field instanceof Street && ((Street) field).getStreetGameColor().equals(this.streetGameColor)))
                    i++;
            }
        }
        return i;
    }

    @Override
    public long getRent(Player player)
    {
        if (super.getOwner().getOwnedStreetAmountByColor(streetGameColor) == getAmountOfStreets())
            return super.getRent(player) * 2;
        else
            return super.getRent(player);

        //TODO HOUSE

    }

    public GameColor getStreetGameColor()
    {
        return streetGameColor;
    }

    public boolean hasHouse()
    {
        return houseCount != 0;
    }
}
