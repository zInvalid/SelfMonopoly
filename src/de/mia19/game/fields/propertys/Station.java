package de.mia19.game.fields.propertys;

import de.mia19.game.objects.Player;

public class Station extends Property
{
    public Station(String name, long price, long rent, int location, int coord)
    {
        super(name, price, rent, location, coord);
    }

    @Override
    public long getRent(Player player)
    {
        int i = 0;
        for(Property property : super.getOwner().getOwnedPropertys())
        {
            if(property instanceof Station)
                i++;
        }
        if(i < 4)
            return super.getRent(player) * i;
        else
            return super.getPrice();
    }

}
