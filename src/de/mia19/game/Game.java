package de.mia19.game;

import de.mia19.game.fields.Field;
import de.mia19.game.fields.corner.FreeParking;
import de.mia19.game.fields.corner.Jail;
import de.mia19.game.fields.corner.Start;
import de.mia19.game.fields.corner.StraightIntoJail;
import de.mia19.game.fields.other.CommunityCard;
import de.mia19.game.fields.other.EventCard;
import de.mia19.game.fields.other.Tax;
import de.mia19.game.fields.propertys.Plant;
import de.mia19.game.fields.propertys.Station;
import de.mia19.game.fields.propertys.Street;
import de.mia19.game.objects.Player;

import java.awt.event.ActionListener;
import java.util.ArrayList;

public class Game
{
    public final ArrayList<Player> players;
    private final ArrayList<Field> fields;
    public long tax = -1;

    public Player activePlayer;

    private static Game instance;

    public static Game getInstance()
    {
        return instance;
    }

    public ArrayList<Field> getFields()
    {
        return fields;
    }

    //public static Game getInstance()
   // {
   //     return  (instance == null) ? (instance = new Game()) : instance;
   // }

    public Game()
    {
        instance = this;

        fields = new ArrayList<>();
        players = new ArrayList<>();

        if (Config.getInstance().isFree_parking())
            tax = 0;

        addFields();
    }

    //TODO HOUSE PRICE AND TAX
    private void addFields()
    {
        fields.add(new Start("LOS", Field.SOUTH, 10));
        fields.add(new Street("Badstraße", 60, 2, GameColor.PURPLE, 0, Field.SOUTH, 9));
        fields.add(new CommunityCard("Gemeinschaftsfeld", Field.SOUTH, 8));
        fields.add(new Street("Turmstraße", 60, 4, GameColor.PURPLE, 0, Field.SOUTH, 7));
        fields.add(new Tax("Einkommensteuer", 0, Field.SOUTH, 6));
        fields.add(new Station("Südbahnhof", 200, 25, Field.SOUTH, 5));
        fields.add(new Street("Chausseestraße", 100, 6, GameColor.LIGHT_BLUE, 0, Field.SOUTH, 4));
        fields.add(new Street("Elisenstraße", 100, 6, GameColor.LIGHT_BLUE, 0, Field.SOUTH, 3));
        fields.add(new EventCard("Ereignisfeld", Field.SOUTH, 2));
        fields.add(new Street("Poststraße", 120, 8, GameColor.LIGHT_BLUE, 0, Field.SOUTH, 1));
        fields.add(new Jail("Im Gefängnis / Nur zu Besuch", Field.SOUTH, 0));

        fields.add(new Street("Seestraße", 140, 10, GameColor.PURPLE, 0, Field.WEST, 9));
        fields.add(new Plant("Elektrizitätswerk", 150,0, Field.WEST, 8));
        fields.add(new Street("Hafenstraße", 140, 10, GameColor.PURPLE, 0, Field.WEST, 7));
        fields.add(new Street("Neue Straße", 160, 12, GameColor.PURPLE, 0, Field.WEST, 6));
        fields.add(new Station("Westbahnhof", 200, 25, Field.WEST, 5));
        fields.add(new Street("Münchener Straße", 180, 14, GameColor.ORANGE, 0, Field.WEST, 4));
        fields.add(new Street("Elisenstraße", 100, 14, GameColor.ORANGE, 0, Field.WEST, 3));
        fields.add(new EventCard("Ereignisfeld", Field.WEST, 2));
        fields.add(new Street("Poststraße", 120, 16, GameColor.ORANGE, 0, Field.WEST, 1));

        fields.add(new FreeParking("Frei Park", Field.NORTH, 0));
        fields.add(new Street("Theaterstraße", 220, 18, GameColor.RED, 0, Field.NORTH, 1));
        fields.add(new EventCard("Ereignisfeld", Field.NORTH, 2));
        fields.add(new Street("Museumstraße", 220, 18, GameColor.RED, 0, Field.NORTH, 3));
        fields.add(new Street("Opernplatz", 240, 20, GameColor.RED, 0, Field.NORTH, 4));
        fields.add(new Station("Nordbanhof", 200, 25, Field.NORTH, 5));
        fields.add(new Street("Lessingstraße", 260, 22, GameColor.YELLOW, 0, Field.NORTH, 6));
        fields.add(new Street("Elisenstraße", 260, 22, GameColor.YELLOW, 0, Field.NORTH, 7));
        fields.add(new Plant("Wasserwerk", 150,0, Field.NORTH, 8));
        fields.add(new Street("Goethestraße", 280, 24, GameColor.YELLOW, 0, Field.NORTH, 9));
        fields.add(new StraightIntoJail("Gehen Sie in das Gefängnis", Field.NORTH, 10));

        fields.add(new Street("Schlossallee", 400, 50, GameColor.BLUE, 0, Field.EAST, 9));
        fields.add(new Tax("Zusatzsteuer", 0, Field.EAST, 8));
        fields.add(new Street("Parkstraße", 350, 35, GameColor.BLUE, 0, Field.EAST, 7));
        fields.add(new EventCard("Ereignisfeld", Field.EAST, 6));
        fields.add(new Station("Hauptbahnhof", 200, 25, Field.EAST, 5));
        fields.add(new Street("Bahnhofstraße", 320, 28, GameColor.GREEN, 0, Field.EAST, 4));
        fields.add(new CommunityCard("Elisenstraße", Field.EAST, 3));
        fields.add(new Street("Hauptstraße", 300, 26, GameColor.GREEN, 0, Field.EAST, 2));
        fields.add(new Street("Rathausplatz", 300, 26, GameColor.GREEN, 0, Field.EAST, 1));
    }

    public ActionListener diceListener = (ae) ->
    {
        if (!activePlayer.roll())
        {
            //TODO NEXT PLAYER
        }
        //Field.getFromNumber(activePlayer.getPosition());
    };


}
