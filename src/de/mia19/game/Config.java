package de.mia19.game;

public class Config
{
    private long start_money = 200;
    private boolean go_double_money = true;
    private boolean free_parking = true;

    private static Config instance;

    public static Config getInstance()
    {
        return (instance == null) ? (instance = new Config()) : instance;
    }

    private Config()
    {
    }

    public long getStart_money()
    {
        return start_money;
    }

    public void setStart_money(long start_money)
    {
        this.start_money = start_money;
    }

    public boolean isGo_double_money()
    {
        return go_double_money;
    }

    public void setGo_double_money(boolean go_double_money)
    {
        this.go_double_money = go_double_money;
    }

    public boolean isFree_parking()
    {
        return free_parking;
    }

    public void setFree_parking(boolean free_parking)
    {
        this.free_parking = free_parking;
    }
}
