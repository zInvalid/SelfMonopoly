package de.mia19.game.objects;

import de.mia19.game.*;
import de.mia19.game.fields.Field;
import de.mia19.game.fields.propertys.Property;
import de.mia19.game.fields.propertys.Street;

import java.util.ArrayList;

public class Player
{
    private final GameColor gameColor;

    private long money;
    private int position;

    private boolean inJail;
    private int roundsInJail;

    private Dice dice;

    private final ArrayList<Property> ownedPropertys;

    public Player(GameColor gameColor)
    {
        this.gameColor = gameColor;
        this.ownedPropertys = new ArrayList<>();
        this.dice = new Dice();
        this.money = 0;
        this.position = 0;
        this.inJail = false;
        this.roundsInJail = 0;
    }

    public ArrayList<Property> getOwnedByType(Class<?> cls)
    {
        ArrayList<Property> temp = new ArrayList<>();
        for(Property property : ownedPropertys)
        {
            if(cls.isInstance(property))
                temp.add(property);
        }


        return temp;
    }

    public int getOwnedStreetAmountByColor(GameColor gameColor)
    {
        int i = 0;
        for (Property property : getOwnedByType(Street.class))
        {
            if (((Street) property).getStreetGameColor().equals(gameColor))
                i++;
        }
        return i;
    }

    private boolean ownsStreet()
    {
        for (Property property : ownedPropertys)
        {
            if(property instanceof Street)
                return true;
        }

        return false;
    }

    public ArrayList<Property> getOwnedPropertys()
    {
        return ownedPropertys;
    }

    public boolean buyProperty(Property property)
    {
        if (property.getPrice() <= money)
        {
            removeMoney(property.getPrice());
            property.setOwner(this);
            ownedPropertys.add(property);
            return true;
        }
        return false;
    }

    public void pay(Player to, long amount)
    {
        if ((money - amount) < 0)
        {
            to.addMoney(money);
            removeMoney(amount);
            lose();
        }
        else
        {
            to.addMoney(amount);
            removeMoney(amount);
        }
    }

    public void pay_tax(long amount)
    {
        if ((money - amount) < 0)
        {
            removeMoney(money);
            lose();
            if (Config.getInstance().isFree_parking())
                Game.getInstance().tax += money;
        }
        else
        {
            removeMoney(amount);
            if (Config.getInstance().isFree_parking())
                Game.getInstance().tax += amount;
        }
    }

    //TODO JAIL

    /**
     *
     * @return if a player is allowed to roll again
     */
    public boolean roll()
    {
        if (isInJail())
        {
            //TODO ASK PAY OR ROLL
            if (true)
            {
                //TODO PRICE
                pay_tax(100);

                setInJail(false);
                dice.roll();
                moveToField();
                return true;
            }
            else
            {
                dice.roll();
                if (dice.isDouble())
                {
                    setInJail(false);
                    moveToField();
                    return true;
                }
            }
            return false;
        }
        else
        {
            dice.roll();
            moveToField();
            if (dice.isDouble())
            {
                if (dice.getDoubleInARow() == 3)
                {
                    setInJail(true);
                    return true;
                }
            }
            return false;
        }
    }

    private void moveToField()
    {
        int moves = dice.getValue();
        if ((position + moves) > 39)
        {
            moves -= (40 - position);
            this.position = 0;
        }
        this.position += moves;

        //Action
        Field.getFromNumber(this.position).action(this);
    }

    //TODO LOSE
    public void lose()
    {

    }

    public int getPosition()
    {
        return position;
    }

    public void addMoney(long money)
    {
        this.money += money;
    }

    public void removeMoney(long amount)
    {
        this.money -= money;
    }

    public Dice getDice()
    {
        return dice;
    }

    public boolean isInJail()
    {
        return inJail;
    }

    public void setInJail(boolean inJail)
    {
        this.inJail = inJail;
    }

}
