package de.mia19.game;

import java.awt.*;

/**
 * Explicit colors for monopoly
 */
public enum GameColor
{
    PURPLE("purple", new Color(76, 0 ,153)),
    LIGHT_BLUE("light_blue", new Color(127, 178, 255)),
    LIGHT_PURPLE("light_purple", Color.PINK),
    ORANGE("orange", Color.ORANGE),
    RED("red", Color.RED),
    YELLOW("yellow", Color.YELLOW),
    GREEN("green", Color.GREEN),
    BLUE("blue", Color.BLUE);

    private String name;
    private Color color;

    GameColor(String s, Color color)
    {
        this.name = s;
        this.color = color;
    }

    public static GameColor parseString(String s)
    {
        for(GameColor c : GameColor.values())
        {
            if(c.getName().equalsIgnoreCase(s))
                return c;
        }
        return null;
    }

    public String getName()
    {
        return name;
    }

    public Color getColor()
    {
        return color;
    }

}
