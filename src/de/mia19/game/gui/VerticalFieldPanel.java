package de.mia19.game.gui;

import de.mia19.game.fields.Field;
import de.mia19.game.fields.propertys.Plant;
import de.mia19.game.fields.propertys.Property;
import de.mia19.game.fields.propertys.Station;
import de.mia19.game.fields.propertys.Street;

import javax.swing.*;
import java.awt.*;

public class VerticalFieldPanel extends JPanel
{
    public VerticalFieldPanel(Field field)
    {
        this.add(new JLabel(field.getName()));

        this.setBorder(BorderFactory
                .createLineBorder(Color.BLACK));
        if (field instanceof Property)
        {
            if (field instanceof Street)
            {

            }
            else if (field instanceof Station)
            {

            }
            else if (field instanceof Plant)
            {

            }
        }
        else
        {
        }
    }
}
