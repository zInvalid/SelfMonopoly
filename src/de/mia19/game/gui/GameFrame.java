package de.mia19.game.gui;

import javax.swing.*;

public class GameFrame extends JFrame
{
    JPanel midPanel;

    public GameFrame()
    {
        this.add(new BoardPanel());

                /* JFRAME SETTINGS */
        this.setSize(1200, 830);
        this.setResizable(true);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }
}
