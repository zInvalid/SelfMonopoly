package de.mia19.game.gui;

import de.mia19.game.fields.Field;
import de.mia19.game.fields.propertys.Plant;
import de.mia19.game.fields.propertys.Property;
import de.mia19.game.fields.propertys.Station;
import de.mia19.game.fields.propertys.Street;

import javax.swing.*;
import java.awt.*;

public class HorizontalFieldPanel extends JPanel
{
    public HorizontalFieldPanel(Field field)
    {

        this.setBorder(BorderFactory
                .createLineBorder(Color.BLACK));
        if (field instanceof Property)
        {
            if (field instanceof Street)
            {
                JLabel color = new JLabel("testest");
                color.setBackground(((Street) field).getStreetGameColor().getColor());
                color.setOpaque(true);

                this.add(color, BorderLayout.NORTH);
            }
            else if (field instanceof Station)
            {

            }
            else if (field instanceof Plant)
            {

            }
        }
        else
        {
            this.add(new JLabel(field.getName()));
        }
    }
}
