package de.mia19.game.gui;

import de.mia19.game.fields.Field;

import javax.swing.*;
import java.awt.*;

public class BoardPanel extends JPanel
{
    public BoardPanel()
    {
        final GridBagLayout layout = new GridBagLayout();

        final double[] weights = new double[]{0.2, 0.1, 0.1, 0.1, 0.1,
                0.1, 0.1, 0.1, 0.1, 0.1, 0.2};
        layout.rowWeights = weights;
        layout.columnWeights = weights;
        this.setLayout(layout);

        GridBagConstraints c = new GridBagConstraints();

        int gridX = 0;
        int gridY = 0;

        for (int i = 0; i < 2; i++)
        {
            for (int x = 0; x < 11; x++)
            {
                switch (i)
                {
                    case 0://Top Spaces
                        gridX = x;
                        gridY = 0;
                        break;
                    case 1://Bottom Spaces
                        gridX = x;
                        gridY = 10;
                        break;
                }
                c.gridx = gridX;
                c.gridy = gridY;
                c.fill = GridBagConstraints.BOTH;

                this.add(new HorizontalFieldPanel(Field.getFromCoords(i, x)), c);
            }
        }

        for (int i = 2; i < 4; i++)
        {
            for (int y = 1; y < 10; y++)
            {
                switch (i)
                {
                    case 2://Right Spaces
                        gridX = 10;
                        gridY = y;
                        break;
                    case 3://Left Spaces
                        gridX = 0;
                        gridY = y;
                        break;
                }
                c.gridx = gridX;
                c.gridy = gridY;
                c.fill = GridBagConstraints.BOTH;
                this.add(new VerticalFieldPanel(Field.getFromCoords(i, y)), c);
            }
        }
    }
}
