package de.mia19;

import java.awt.*;

public class RessourceLoader
{
    static RessourceLoader instance = new RessourceLoader();

    /**
     * Returns an Image object that can then be painted on the screen.
     *
     * @param  file filename + type
     * @return      the image with the name
     * @see         Image
     */
    public static Image getImage(String file)
    {
        return Toolkit.getDefaultToolkit().getImage(instance.getClass().getResource("img/" + file));
    }
}
